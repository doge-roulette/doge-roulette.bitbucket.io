import "./App.css";
import { Fragment, useCallback, useMemo, useState } from "react";

const COLORS = ["#2F6057", "#54A76F", "#EAC154", "#F75C08", "#D94532", "#EF798A", "#F7A9A8", "#7D82B8", "#613F75", "#E5C3D1"];
const INITIAL_VALUE = `{"Player 1":0.25, "Player2":0.5, "Player3":0.125, "Player4":0.125}`;

function getRadius() {
  const r =
    window.innerWidth > window.innerHeight
      ? window.innerHeight
      : window.innerWidth;

  return r * 0.4;
}

function getWinner(rotation, parsedValue) {
  const rotationPercentage = (360 - (rotation % 360)) / 360;

  return Object.entries(parsedValue)
    .reduce((acc, [text, value], i) => {
      const prevMaxValue = acc[i - 1] ? acc[i - 1][1][1] : 0;
      return [...acc, [text, [prevMaxValue, prevMaxValue + value]]];
    }, [])
    .filter(
      ([, [min, max]]) => rotationPercentage >= min && rotationPercentage < max
    )
    .map(([text]) => text)[0];
}

function App() {
  const [radius] = useState(getRadius);
  const [inputValue, setInputValue] = useState(INITIAL_VALUE);
  const [duration, setDuration] = useState(0);
  const [rotation, setRotation] = useState(0);
  const [isWinnerDisplayed, setWinnerDisplay] = useState(false);

  const parsedValue = useMemo(() => {
    try {
      return JSON.parse(inputValue);
    } catch (error) {
      return {};
    }
  }, [inputValue]);

  const winner = useMemo(() => getWinner(rotation, parsedValue), [
    rotation,
    parsedValue,
  ]);

  const handleClick = useCallback(() => {
    setWinnerDisplay(false);
    // RNG
    const randomNumber = 1 + Math.random();
    const newDuration = 8000 * randomNumber;

    setDuration(() => newDuration);
    setRotation((value) => value + 3600 * randomNumber);

    setTimeout(() => {
      setWinnerDisplay(true);
    }, newDuration);
  }, []);

  console.log(winner);

  return (
    <div className="App">
      <div className="input-area">
        {isWinnerDisplayed && (
          <div>
            <strong>{winner}</strong> wonnerinoed
          </div>
        )}
        <button type="button" onClick={handleClick}>
          SPIN
        </button>
        <textarea
          name="input"
          id="input"
          value={inputValue}
          onChange={(e) => {
            setInputValue(e.target.value);
          }}
        />
      </div>
      <div
        style={{
          position: "relative",
          width: 2 * radius,
          height: 2 * radius,
          overflow: "hidden",
        }}
      >
        <svg
          height={2 * radius}
          width={2 * radius}
          viewBox={`0 0 ${2 * radius} ${2 * radius}`}
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            transformOrigin: "center",
            transform: `rotate(${rotation}deg)`,
            transition: `transform ${duration.toFixed(
              0
            )}ms cubic-bezier(0.15, 0.3, 0, 1)`,
          }}
        >
          <g></g>
          <circle r={radius} cx={radius} cy={radius} fill="lightgray" />
          {Object.entries(parsedValue).map(([text, value], i, list) =>
            typeof value !== "number" ? null : (
              <circle
                key={text}
                r={radius / 2}
                cx={radius}
                cy={radius}
                fill="transparent"
                stroke={COLORS[i]}
                strokeWidth={radius}
                strokeDasharray={`${Math.PI * radius * value} ${radius * 9999}`}
                transform={`rotate(${list
                  .slice(0, i)
                  .reduce((acc, [, value]) => acc + value * 360, -90)})`}
                transform-origin="center"
              />
            )
          )}
          {Object.entries(parsedValue).map(([text, value], i, list) => {
            if (typeof value !== "number") {
              return null;
            }

            const rotateDeg = list
              .slice(0, i)
              .reduce((acc, [, value]) => acc + value * 360, value * 180);

            return (
              <Fragment key={text}>
                <text
                  x={radius}
                  y={radius / 3}
                  stroke="white"
                  fill="white"
                  textAnchor="middle"
                  transform={`rotate(${rotateDeg})`}
                  transform-origin="center"
                  fontSize="16px"
                >
                  {text}
                </text>
                <text
                  x={radius}
                  y={radius / 3 + 20}
                  stroke="white"
                  fill="white"
                  textAnchor="middle"
                  transform={`rotate(${rotateDeg})`}
                  transform-origin="center"
                  fontSize="12px"
                >
                  {(value * 100).toFixed(2)}%
                </text>
              </Fragment>
            );
          })}
        </svg>
        <svg
          height={2 * radius}
          width={2 * radius}
          viewBox={`0 0 ${2 * radius} ${2 * radius}`}
          style={{
            position: "absolute",
            top: -10,
            left: 0,
          }}
        >
          <polygon
            points={`${radius - 12},0 ${radius + 12},0 ${radius},48`}
            fill="black"
          />
        </svg>
      </div>
    </div>
  );
}

export default App;
